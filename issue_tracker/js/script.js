// Getting components
const title = document.getElementById("title")
const description = document.getElementById("description")
const progress = document.getElementById("progress")
const link = document.getElementById("link")
const issuesView = document.getElementById("issues-view")
const saveLinkBtn = document.getElementById("save-link-btn")
const addLinkBtn = document.getElementById("add-link-btn")
const addIssueBtn = document.getElementById("add-issue-btn")
const allInputFields = document.getElementsByClassName("input")
const existingTickets = JSON.parse(localStorage.getItem("issue-tickets"))
let ticketsList = []

/**
 * Load tickets that are in localStorage when starting
 * 
 */
if (existingTickets) {
  ticketsList = existingTickets
  renderView(ticketsList)
}

/**
 * Get values: title, description, option and link and add them to list
 * Render the values in the issues view
 * don't forget to clear the input fields
 * Add items to the DOM
 * *** ensure fields are not empty before adding
 */
addIssueBtn.addEventListener("click", function() {
  ticketsList.push({
    ticketIssueId: randstr("Ticket: "),
    ticketProgress: progress.value,
    ticketTitle: title.value,
    ticketDescription: description.value,
    ticketLink: link.value
  })

  progress.value = ""
  title.value = ""
  description.value = ""
  link.value = ""

  localStorage.setItem("issue-tickets", JSON.stringify(ticketsList))
  renderView(ticketsList)
})

/**
 * change progress to resolved if close btn is clicked
 * Remove item from localStorage and DOM when delete is clicked
 */
 issuesView.addEventListener("click", function(e){ 
  if (e.target.id === "close") { 
    const id = document.getElementById("issue-id").textContent 
    closeTicket(id)
  }
})

issuesView.addEventListener("click", function(e){ 
  if (e.target.id === "delete") { 
    const id = document.getElementById("issue-id").textContent 
    deleteTicket(id)
  }
})

saveLinkBtn.addEventListener("click", function() {  
    chrome.tabs.query({active: true, currentWindow: true}, function(tabs){
        link.value = tabs[0].url
    })
})

function renderView(tickets) {
  let views = ""
  for (let i = 0; i < tickets.length; i++) {
    views += `
      <div class="issues">
        <p id="issue-id">${tickets[i].ticketIssueId}</p>
        <p>${tickets[i].ticketProgress} </p>
        <h3>${tickets[i].ticketTitle}</h3>
        <p>${tickets[i].ticketDescription}</p>
        <a href=${tickets[i].ticketLink} target="_blank">${tickets[i].ticketLink}</a>
        <div class="views-btn">
          <button id="close"> Close </button>
          <button id="delete"> Delete </button>
        </div>
      </div>`
  } 
  issuesView.innerHTML += views 
}



// helper funs
//generate random issueNO(aid when closing and deleting)
function randstr(prefix) {
    return Math.random().toString(36).replace('0.',prefix || '');
}

function closeTicket(id) {
  for(let i = 0; i < ticketsList.length; i++) {
    if (ticketsList[i].ticketIssueId == id) {
      ticketsList[i].ticketProgress = "resolved"
    }
  }
  localStorage.setItem("issue-tickets", JSON.stringify(ticketsList))
  renderView(ticketsList)
  window.location.reload()
}

function deleteTicket(id) {
  for(let i = 0; i < ticketsList.length; i++) {
    if (ticketsList[i].ticketIssueId == id) {
      ticketsList.splice(i, 1)
    }
  }
  localStorage.setItem("issue-tickets", JSON.stringify(ticketsList))
  renderView(ticketsList)
  window.location.reload()
}

/**
 * TO ADD
 * Update views without having to reload window
 */
